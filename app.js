/**
 * INCLUDE LIBRARY
 */
const express = require('express');
const morgan = require('morgan');
const winston = require('winston');
const path = require('path');
const bodyParser = require('body-parser');
const config = require('./config'); // get our config file
const app = express();
const cors = require('cors');
const helmet = require('helmet');
const ev = require('express-validation');
const {
    ExceptionMapper,
    SystemError
} = require('./src/base/exception');
const {Response} = require('./src/base/base.response');

/**
 * 
 * KONFIGURASI 
 */
const logger = new winston.Logger({
    transports: [
        //   new winston.transports.File({
        //       level: 'info',
        //       filename: './logs/all-logs.log',
        //       handleExceptions: true,
        //       json: true,
        //       maxsize: 5242880, //5MB
        //       maxFiles: 5,
        //       colorize: false
        //   }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ],
    exitOnError: false
});

logger.stream = {
    write: function (message, encoding) {
        logger.info(message);
    }
};
// app.use(expressValidator());
app.use(cors());
app.use(morgan('combined', {
    stream: logger.stream
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(helmet());
app.set('secret', config.secret);
app.disable('x-powered-by');
app.use(express.static(path.join(__dirname, 'public/apidoc/')));
app.get('/apidoc/*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/apidoc/index.html'));
});
require('./src/routes/public')(app);
require('./src/routes/protected')(app);
app.use((err, req, res, next) => {
    if (err instanceof ev.ValidationError) return res.status(err.status).json(Object.assign(Response, {
        statusCode: err.status,
        publicMessage: err.message,
        developerMessage: err.message,
        validationError: err.errors
    }));
    if (process.env.NODE_ENV !== 'production') {
        let send = false;
        ExceptionMapper.forEach(element => {
            if (err instanceof element) {
                send = true;
                return res.status(err.statusCode).json(err.toJson());
            }
        });
        if(!send){
            let sysError = new SystemError();
            return res.status(sysError.statusCode).json(sysError.toJson());
        }
    } else {
        ExceptionMapper.forEach(element => {
            if (err instanceof element) {
                return res.status(err.statusCode).json(err.toJson());
            }
        });
        return res.status(500).send(err.stack);
    }
});

app._router.stack.forEach(function (r) {
    if (r.route && r.route.path) {
        console.log(r.route.path)
    }
});
module.exports = app;