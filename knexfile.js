// Update with your config settings.
module.exports = {
  development: {
    client: 'pg',
    connection: {
      host : '127.0.0.1',
      database: 'emikro',
      user:     'postgres',
      password: '1234'
    },
    pool: {
      min: 2,
      max: 10
    },
    debug: true
  },
  staging: {
    client: 'pg',
    connection: {
      host : '103.15.226.134',
      port: 5432,
        database: process.env.DB_NAME,
        user: process.env.DB_USER,
        password: process.env.DB_PASS
    },
    pool: {
      min: 2,
      max: 10
    },
    debug: true
  },
    test: {
        client: 'pg',
        connection: {
            host: '127.0.0.1',
            database: 'emikro-test',
            user: 'postgres',
            password: '1234'
        },
        pool: {
            min: 2,
            max: 10
        }
    },
  production: {
    client: 'pg',
    connection: {
      host : '127.0.0.1',
      database: 'emikro',
      user:     'postgres',
      password: '1234'
    },
    pool: {
      min: 2,
      max: 10
    }
  }
};
