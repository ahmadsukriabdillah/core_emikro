const users = require("../controllers").users;
const database = require('./../utils/db');
const {SystemError} = require('./../base/exception');
const validate = require('express-validation');
const Joi = require('joi');

module.exports = (app) => {
    /**
     * @apiDefine GlobalError
     @apiError ValidationError Required payload must be missing
     @apiErrorExample {json} Bad-Request-Error-Response
     HTTP/1.1 400 Bad Request:
     {
         "statusCode": 400,
         "publicMessage": "validation error",
         "developerMessage": "validation error",
         "validationError": [
             {
                 "field": "username",
                 "location": "body",
                 "messages": [
                     "\"username\" is required"
                 ],
                 "types": [
                     "any.required"
                 ]
             },
             {
                 "field": "password",
                 "location": "body",
                 "messages": [
                     "\"password\" is required"
                 ],
                 "types": [
                     "any.required"
                 ]
             }
         ]
     }
     @apiError UnauthorizationRequest Required payload must be missing
     @apiErrorExample {json} Unauthorization-Error-Response
     HTTP/1.1 401 Unauthorization Request:
     {
         "statusCode": 401,
         "publicMessage": "Username / Password salah.",
         "developerMessage": "Unatuhoriztion request.",
     }
     */
    /**
     * @api {post} /api/auth Login
     * @apiVersion 1.0.0
     * @apiName Login to get Access token
     * @apiGroup Authentication
     * @apiParam {String} username Username user.
     * @apiParam {String} password Password user.
     * @apiSuccessExample {json} Login Success:
     *      HTTP/1.1 200 OK:
     *      {
     *          "statusCode": 200,
     *           "publicMessage": "Succesfull",
     *           "developerMessage": "Succesfull",
     *           "data": {
     *               "profile": {
     *                   "uid": "admin",
     *                   "nama": "ADMINISTRATOR",
     *                   "lvl": "9",
     *                   "level": "ADMINISTRATOR",
     *                   "bagian": "-- Pilih Bagian --",
     *                   "jabatan": "IT SUPPORT",
     *                   "administrator": 1,
     *                   "setting": 1,
     *                   "master_data": 1,
     *                   "md_anggota": 1,
     *                   "md_usp": 1,
     *                   "md_dagang": 1,
     *                   "md_jasa": 1,
     *                   "md_sewa": 1,
     *                   "md_ppob": 1,
     *                   "md_lap_gabungan": 1,
     *                   "md_pembagian_shu": 1,
     *                   "st": 0,
     *                   "act": 1,
     *                   "err_pass": 0,
     *                   "chg_pass": 0,
     *                   "cif": "00040",
     *                   "uid_crt": "",
     *                   "time_stamp": "2017-03-09T05:42:06.000Z"
     *               },
     *               "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJhZG1pbiIsIm5hbWEiOiJBRE1JTklTVFJBVE9SIiwibHZsIjoiOSIsImxldmVsIjoiQURNSU5JU1RSQVRPUiIsImJhZ2lhbiI6Ii0tIFBpbGloIEJhZ2lhbiAtLSIsImphYmF0YW4iOiJJVCBTVVBQT1JUIiwiYWRtaW5pc3RyYXRvciI6MSwic2V0dGluZyI6MSwibWFzdGVyX2RhdGEiOjEsIm1kX2FuZ2dvdGEiOjEsIm1kX3VzcCI6MSwibWRfZGFnYW5nIjoxLCJtZF9qYXNhIjoxLCJtZF9zZXdhIjoxLCJtZF9wcG9iIjoxLCJtZF9sYXBfZ2FidW5nYW4iOjEsIm1kX3BlbWJhZ2lhbl9zaHUiOjEsInN0IjowLCJhY3QiOjEsImVycl9wYXNzIjowLCJjaGdfcGFzcyI6MCwiY2lmIjoiMDAwNDAiLCJ1aWRfY3J0IjoiIiwidGltZV9zdGFtcCI6IjIwMTctMDMtMDlUMDU6NDI6MDYuMDAwWiIsImlhdCI6MTUyNDMxMzczMiwiZXhwIjoxNTI0MzI0NTMyfQ.S3DLHsgq4qGSDw58pi-u4hbKhnxNbqVaMwPZju1z_NA"
     *           }
     *       }
     * @apiUse GlobalError
     *
     */
    app.post('/api/auth', validate({
        body: {
            username: Joi.string().required(),
            password: Joi.string().required()
        }
    }), users.auth);
    /**
     * @api {get} /api/verify Get Status Server
     * @apiVersion 1.0.0
     * @apiGroup Utils
     * @apiName Get Status Server
     * @apiSuccessExample {json} Server OK:
     *      HTTP/1.1 200 OK:
     *      {
     *          "statusCode": 200,
     *          "publicMessage": "Succesfull"
     *      }
     * @apiErrorExample {json} Server Error:
     *      HTTP/1.1 400 BAD REQUEST:
     *      {
     *          "statusCode": 400,
     *          "publicMessage": "Failed."
     *      }
     */
    app.get('/api/verify', (req, res) => {
        database.raw('select 1').then((resp) => {
            res.status(200).json({
                statusCode: 200,
                publicMessage: "Succesfull"
            });
        }).catch((err) => {
            throw new SystemError("Failed");
        });
    });
    /**
     * @api {get} /api/desktop/version Get Version
     * @apiVersion 1.0.0
     * @apiGroup Utils
     * @apiName Get Lastes Version Application
     * @apiSuccessExample {json} Get Version Success:
     *      HTTP/1.1 200 OK:
     *      {
     *          "statusCode": 200,
     *          "publicMessage": "Succesfull",
     *          "version": {
     *              "jenis": "MITRA",
     *              "versi_apl": "G2",
     *              "cif": "00040"
     *          }
     *      }
     *
     */
    app.get('/api/desktop/version', (req, res, next) => {
        database.from('dt_versi_apl')
            .orderBy("versi_apl", "DESC")
            .limit(1)
            .where("jenis", "MITRA")
            .first()
            .then((resp) => {
                res.status(200).json({
                    statusCode: 200,
                    publicMessage: "Succesfull",
                    version: resp
                });
            }).catch((err) => {
            console.log(err);
            next(new SystemError("Failed"));
        });
    });
};