const md5 = require('md5');
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('./../../config');
const database = require('./../utils/db');
const {
    ValidationException,
    UnauthorizerException,
    SystemError,
    FailedTaskExecution
} = require('./../base/exception');
module.exports = {
    auth(req, res, next) {
        const {
            username,
            password
        } = req.body;
        database
            .from("uid")
            .select()
            .where("uid", username || "")
            .first()
            .then((result) => {
                if (result == null) {
                    return next(new UnauthorizerException("username / password anda salah"));
                }
                if (result.st <= 0) {
                    return next(new UnauthorizerException("User need to activeate"));
                }
                if (result.err_pass >= 3) {
                    return next(new UnauthorizerException("User Has been suspended"));
                }
                if (md5(password) == result.password) {
                    delete result.password;
                    database("uid")
                        .where("uid", result.uid)
                        .update({
                            err_pass: 0
                        }).then((resp) => {
                            let token = jwt.sign(result, config.secret, {
                                expiresIn: '3h'
                            });
                            res.status(200).json({
                                statusCode: 200,
                                publicMessage: "Succesfull",
                                developerMessage: "Succesfull",
                                data: {
                                    profile: result,
                                    token: token
                                }
                            });
                        });
                } else {
                    database.raw("update uid set err_pass = err_pass + 1 where uid = ?", [result.uid])
                        .then(resp => {

                        }).catch(err => {
                        });
                    return next(new UnauthorizerException("username / password anda salah"));
                }
            })
            .catch((err) => {
                let exception = new FailedTaskExecution("Action Can't processed.", err.message);
                next(exception);
            });
    },
    changePassword(req, res, next) {
        const {
            uid,
            cif
        } = req.decoded;
        const {
            oldPassword,
            newPassword,
            confirmPassword
        } = req.body;
        database
            .from("uid")
            .select()
            .where("uid", uid)
            .first()
            .then((result) => {
                if (result == null) {
                    return next(new FailedTaskExecution("Action Can't processed.", err.message));
                }

                if (md5(oldPassword) == result.password) {
                    delete result.password;
                    database("uid")
                        .where("cif", cif)
                        .andWhere("uid", uid)
                        .update({
                            password: md5(newPassword)
                        })
                        .then((resp) => {
                            delete result.password;
                            let token = jwt.sign(result, config.secret, {
                                expiresIn: '3h'
                            });
                            res.status(200).json({
                                statusCode: 200,
                                publicMessage: "Succesfull",
                                developerMessage: "Succesfull",
                                data: {
                                    profile: result,
                                    token: token
                                }
                            });
                        })
                        .catch((err) => {
                            next(err);
                        });
                } else {
                    return next(new FailedTaskExecution("Action Can't processed.", err.message));
                }
            })
            .catch((err) => {
                return next(new FailedTaskExecution("Action Can't processed.", err.message));
            });

    },
    findAll(req,res,next){
        const{ cif } = req.decoded;
        database("uid")
            .where("cif",cif)
            .select()
            .then(result => {
                res.status(200).json({
                    statusCode: 200,
                    data: result
                });
            }).catch(err => {
                return next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    },
    findOne(req,res,next){
        const{ cif } = req.decoded;
        database("uid")
            .where("cif",cif)
            .andWhere("uid",req.params.userId)
            .select()
            .first()
            .then(result => {
                if(result == undefined){
                    return next(new NotFoundException("Data You Search not Found."));
                }
                res.status(200).json({
                    statusCode: 200,
                    data: result
                });
            }).catch(err => {
                return next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    },
    create(req,res,next){
    
    },
    update(req,res,next){

    },
    delete(req,res,next){

    }
};