const database = require('./../../utils/db');
const {
    ValidationException,
    SystemError,
    FailedTaskExecution
} = require('./../../base/exception');
const {
    pagging
} = require('./../../utils/pagging');
var table_name = "account_bukubesar";
var view_name = "account_bukubesar";
module.exports = {
    findAll(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .select()
            .where("cif", cif)
            .andWhere("acckel", req.params.kelompokId);
        let params = req.query;
        params.order_by = params.order_by ? params.order_by : "accbb";
        params.sort_by = params.sort_by ? params.sort_by : "ASC";
        pagging(sql, params).then(result => {
            sql
                .clearSelect()
                .select()
                .then((data) => {
                    res.status(200).json({
                        statusCode: 200,
                        data: {
                            model: data,
                            pagination: result.page
                        }
                    });
                })
                .catch((err) => {
                    next(new FailedTaskExecution("Action Can't processed.", err.message));
                });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    },
    findOne(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .select()
            .where("cif", cif)
            .andWhere("accbb", req.params.bukubesarId);
        sql
            .first()
            .then((result) => {
                res.status(200).json({
                    statusCode: 200,
                    data: result
                });
            })
            .catch((err) => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    },
    create(req, res, next) {
        const {
            cif
        } = req.decoded;

        let payload = {
            acckel: req.params.kelompokId,
            accbb: req.body.accbb || database.raw(`(SELECT max(accbb)::integer + 1 from ${table_name} where acckel = ? and cif = ?)`, [req.params.kelompokId, cif]),
            bukubesar: req.body.bukubesar,
            kategori: req.body.kategori || 'GL',
            golongan: req.body.golongan || 'USER',
            resiko: req.body.resiko || 0,
            cif: cif
        };
        let sql = database
            .returning(["acckel", "accbb", "bukubesar", "kategori", "golongan", "resiko"])
            .insert(payload)
            .into(table_name)
            .toQuery();
        database.raw(sql)
            .then(resp => {
                res.status(200).json({
                    statusCode: 200,
                    data: resp.rows[0]
                });
            }).catch(err => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            })
    },
    update(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .select()
            .where("cif", cif)
            .andWhere("accbb", req.params.bukubesarId);
        sql
            .first()
            .then((result) => {
                if (result == undefined) {
                    return next(new FailedTaskExecution("Action Can't processed.", "Data Tidak di temukan"));
                } else {
                    let payload = {
                        bukubesar: req.body.bukubesar || result.bukubesar,
                        kategori: req.body.kategori || result.kategori,
                        golongan: req.body.golongan || result.golongan,
                        resiko: req.body.resiko || result.resiko,
                    };
                    database(table_name)
                        .where("cif", cif)
                        .andWhere("accbb", req.params.bukubesarId)
                        .returning(["accbb","bukubesar","kategori","golongan","resiko"])
                        .update(payload).then(resp => {
                            res.status(200).json({
                                statusCode: 200,
                                data: resp
                            });
                        }).catch(err => {
                            next(new FailedTaskExecution("Action Can't processed.", err.message));
                        });
                }

            })
            .catch((err) => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });


    },
    delete(req, res, next) {
        const {
            cif
        } = req.decoded;

        database(table_name)
            .where("cif", cif)
            .andWhere("accbb", req.params.bukubesarId)
            .delete().then(() => {
                res.status(200).json({
                    statusCode: 200,
                    publicMessage: "Succesfull"
                });
            }).catch(err => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    }
};