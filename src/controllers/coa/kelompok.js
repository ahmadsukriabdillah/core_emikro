const database = require('./../../utils/db');
const {
    ValidationException,
    SystemError,
    FailedTaskExecution
} = require('./../../base/exception');
const {
    pagging
} = require('./../../utils/pagging');
var table_name = "account";
var view_name = "account";
module.exports = {
    findAll(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from("account_kel")
            .select()
            .where("cif", cif)
            .andWhere("accjenis", req.params.jenisId);

        let params = req.query;
        params.order_by = params.order_by ? params.order_by : "acckel";
        params.sort_by = params.sort_by ? params.sort_by : "ASC";
        pagging(sql, params).then(result => {
            sql
                .clearSelect()
                .select()
                .then((data) => {
                    res.status(200).json({
                        statusCode: 200,
                        data: {
                            model: data,
                            pagination: result.page
                        }
                    });
                })
                .catch((err) => {
                    next(new FailedTaskExecution("Action Can't processed.", err.message));
                });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    },
    findOne(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from("account_kel")
            .select()
            .where("cif", cif)
            .andWhere("acckel", req.params.kelompokId);
        sql
            .first()
            .then((result) => {
                res.status(200).json({
                    statusCode: 200,
                    data: result
                });
            })
            .catch((err) => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    },
    create(req, res, next) {
        const {
            cif
        } = req.decoded;

        let payload = {
            accjenis: req.params.jenisId,
            acckel: req.body.acckel,
            kelompok: req.body.kelompok,
            gol: req.body.gol,
            subgol: req.body.subgol,
            cif: cif
        };
        database(table_name)
            .insert(payload).then(() => {
            res.status(200).json({
                statusCode: 200,
                data: payload
            });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    },
    update(req, res, next) {
        const {
            cif
        } = req.decoded;

        let payload = {
            kelompok: req.body.kelompok,
            gol: req.body.gol,
            subgol: req.body.subgol
        };
        database(table_name)
            .where("cif", cif)
            .andWhere("acckel", req.params.kelompokId)
            .update(payload).then(() => {
            res.status(200).json({
                statusCode: 200,
                data: payload
            });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    },
    delete(req, res, next) {
        const {
            cif
        } = req.decoded;

        database(table_name)
            .where("cif", cif)
            .andWhere("acckel", req.params.kelompokId)
            .delete().then(() => {
            res.status(200).json({
                statusCode: 200,
                publicMessage: "Succesfull"
            });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    }
};