const database = require('./../utils/db');
const {
    ValidationException,
    SystemError,
    FailedTaskExecution
} = require('./../base/exception');
const moment = require('moment');
const {
    pagging
} = require('./../utils/pagging');
var table_name = "cib";
var view_name = "cib";
module.exports = {
    findAll(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .where("cif", cif);
        let params = req.query;
        params.order_by = params.order_by ? params.order_by : "cib";
        params.sort_by = params.sort_by ? params.sort_by : "ASC";
        pagging(sql, params).then(result => {
            sql
                .clearSelect()
                .select()
                .then((data) => {
                    res.status(200).json({
                        statusCode: 200,
                        data: {
                            model: data,
                            pagination: result.page
                        }
                    });
                })
                .catch((err) => {
                    next(new FailedTaskExecution("Action Can't processed.", err.message));
                });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });

    },
    findOne(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .select()
            .where("cif", cif)
            .andWhere("cib", req.params.cibId)
            .first()
            .then((result) => {
                if (result == undefined) {
                    return next(new FailedTaskExecution("Action Can't processed.", "Data Tidak di temukan"));
                }
                return res.status(200).json({
                    statusCode: 200,
                    data: result
                });
            }).catch(err => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    },
    create(req, res, next) {
        const {
            cif
        } = req.decoded;

        let payload = {
            cib: database.raw("(SELECT lpad((max(cib)::integer + 1)::text,5,'0') from cib where cif = ?)", [cif]),
            nokartu: req.body.nokartu,
            nip: req.body.nip,
            nogaji: req.body.nogaji,
            nama: req.body.nama,
            ki: req.body.ki,
            panggilan: req.body.pangilan,
            kl: req.body.kl,
            tmp_lahir: req.body.tmp_lahir,
            tgl_lahir: req.body.tgl_lahir,
            id: req.body.id,
            no_id: req.body.no_id,
            tglexpired: req.body.tglexpired,
            agama: req.body.agama,
            pendidikan: req.body.pendidikan,
            pekerjaan: req.body.pekerjaan,
            jabatan: req.body.jabatan,
            golongan: req.body.golongan,
            alamat: req.body.alamat,
            telepon: req.body.telepon,
            handphone: req.body.handphone,
            e_mail: req.body.e_mail,
            namaahliwaris: req.body.namaahliwaris,
            hubkel: req.body.hubkel,
            alamatahliwaris: req.body.alamatahliwaris,
            tglbuka: moment().format(),
            st: 0,
            cif: cif
        };
        let sql = database
            .returning(["agama", "alamat", "alamatahliwaris", "cib", "cif", "e_mail", "golongan", "handphone", "hubkel", "id", "jabatan", "ki", "kl", "nama", "namaahliwaris", "nip", "no_id", "nogaji", "nokartu", "pakai_coin", "pakai_plafond", "pakai_voucher", "panggilan", "pekerjaan", "pendidikan", "pokok", "saldo_coin", "saldo_plafond", "saldo_voucher", "st", "tambah_coin", "tambah_plafond", "tambah_voucher", "telepon", "tgl_lahir", "tglbuka", "tglexpired", "tmp_lahir", "total_belanja", "wajib"])
            .insert(payload)
            .into(table_name)
            .toQuery();
        database.raw(sql)
            .then(resp => {
                res.status(200).json({
                    statusCode: 200,
                    data: resp.rows[0]
                });
            }).catch(err => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            })
    },
    update(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .select()
            .where("cif", cif)
            .andWhere("cib", req.params.cibId)
            .first()
            .then((result) => {
                if (result == undefined) {

                    return next(new FailedTaskExecution("Action Can't processed.", "Data Tidak di temukan"));
                }
                let payload = {
                    nokartu: req.body.nokartu || result.nokartu,
                    nip: req.body.nip || result.nip,
                    nogaji: req.body.nogaji || result.nogaji,
                    nama: req.body.nama || result.nama,
                    ki: req.body.ki || result.ki,
                    panggilan: req.body.pangilan || result.pangilan,
                    kl: req.body.kl || result.kl,
                    tmp_lahir: req.body.tmp_lahir || result.tmp_lahir,
                    tgl_lahir: req.body.tgl_lahir || result.tgl_lahir,
                    id: req.body.id || result.id,
                    no_id: req.body.no_id || result.no_id,
                    tglexpired: req.body.tglexpired || result.tglexpired,
                    agama: req.body.agama || result.agama,
                    pendidikan: req.body.pendidikan || result.pendidikan,
                    pekerjaan: req.body.pekerjaan || result.pekerjaan,
                    jabatan: req.body.jabatan || result.jabatan,
                    golongan: req.body.golongan || result.golongan,
                    alamat: req.body.alamat || result.alamat,
                    telepon: req.body.telepon || result.telepon,
                    handphone: req.body.handphone || result.handphone,
                    e_mail: req.body.e_mail || result.e_mail,
                    namaahliwaris: req.body.namaahliwaris || result.namaahliwaris,
                    hubkel: req.body.hubkel || result.hubkel,
                    alamatahliwaris: req.body.alamatahliwaris || result.alamatahliwaris,
                    tglbuka: req.body.tglbuka || result.tglbuka,
                    st: req.body.st || result.st,
                    wajib: req.body.wajib || result.wajib,
                    pokok: req.body.pokok || result.pokok,
                    tambah_voucher: req.body.tambah_voucher || result.tambah_voucher,
                    pakai_voucher: req.body.pakai_voucher || result.pakai_voucher,
                    saldo_voucher: req.body.saldo_voucher || result.saldo_voucher,
                    tambah_plafond: req.body.tambah_plafond || result.tambah_plafond,
                    pakai_plafond: req.body.pakai_plafond || result.pakai_plafond,
                    saldo_plafond: req.body.saldo_plafond || result.saldo_plafond,
                    tambah_coin: req.body.tambah_coin || result.tambah_coin,
                    pakai_coin: req.body.pakai_coin || result.pakai_coin,
                    saldo_coin: req.body.saldo_coin || result.saldo_coin,
                    total_belanja: req.body.total_belanja || result.total_belanja,
                };
                database(table_name)
                    .where("cif", cif)
                    .andWhere("cib", req.params.cibId)
                    .update(payload).then(() => {
                        res.status(200).json({
                            statusCode: 200,
                            publicMessage: 'Succesfull',
                            data: payload
                        });
                    }).catch(err => {
                        next(new FailedTaskExecution("Action Can't processed.", err.message));
                    });
            })
            .catch((err) => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });

    },
    delete(req, res, next) {
        const {
            cif
        } = req.decoded;
        database(table_name)
            .where("cif", cif)
            .andWhere("cib", req.params.cibId)
            .delete().then(() => {
                res.status(200).json({
                    statusCode: 200,
                    publicMessage: "Succesfull"
                });
            }).catch(err => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    },
    approve(req, res, next) {
        const {
            cif
        } = req.decoded;
        database(table_name)
            .where("cif", cif)
            .andWhere("cib", req.params.cibId)
            .update({ st: 2 })
            .then(result => {
                if (result == undefined) {
                    return next(new FailedTaskExecution("Action Can't processed.", "Data Tidak di temukan"));
                }
                return res.status(200).json({
                    statusCode: 200,
                    publicMessage: "Succesfull"
                });
            }).catch(err => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    }
};