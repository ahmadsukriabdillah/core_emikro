const database = require('./../../utils/db');
const {
    ValidationException,
    SystemError,
    FailedTaskExecution
} = require('./../../base/exception');
const {
    pagging
} = require('./../../utils/pagging');
var table_name = "masterpjm";
var view_name = "masterpjm";
module.exports = {
    findAll(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .where("cif", cif);
        let params = req.query;
        params.order_by = params.order_by ? params.order_by : "nopjm";
        params.sort_by = params.sort_by ? params.sort_by : "ASC";
        pagging(sql, params).then(result => {
            sql
                .clearSelect()
                .select()
                .then((data) => {
                    res.status(200).json({
                        statusCode: 200,
                        data: {
                            model: data,
                            pagination: result.page
                        }
                    });
                })
                .catch((err) => {
                    next(new FailedTaskExecution("Action Can't processed.", err.message));
                });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });

    },
    findOne(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .select()
            .where("cif", cif)
            .andWhere("acc", req.params.accountId);
        sql
            .first()
            .then((result) => {
                res.status(200).json({
                    statusCode: 200,
                    data: result
                });
            })
            .catch((err) => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    },
    create(req, res, next) {
        const {
            cif
        } = req.decoded;

        let payload = {
            accbb: req.params.bukubesarId,
            acc: req.body.acc,
            keterangan: req.body.acc,
            golongan: req.body.golongan || 'USER',
            cif: cif
        };
        database(table_name)
            .insert(payload).then(() => {
            res.status(200).json({
                statusCode: 200,
                data: payload
            });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    },
    update(req, res, next) {
        const {
            cif
        } = req.decoded;

        let payload = {
            keterangan: req.body.acc,
            golongan: req.body.golongan || 'USER',
        };
        database(table_name)
            .where("cif", cif)
            .andWhere("acc", req.params.accountId)
            .update(payload).then(() => {
            res.status(200).json({
                statusCode: 200,
                data: payload
            });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    },
    delete(req, res, next) {
        const {
            cif
        } = req.decoded;

        database(table_name)
            .where("cif", cif)
            .andWhere("acc", req.params.accountId)
            .delete().then(() => {
            res.status(200).json({
                statusCode: 200,
                publicMessage: "Succesfull"
            });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    }
};