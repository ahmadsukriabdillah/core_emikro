const users = require('./Users');
const account = require('./coa/account');
const bukubesar = require('./coa/accountbb');
const jenis = require('./coa/jenis');
const kelompok = require('./coa/kelompok');
const anggota = require('./anggota');

const masterDeposito = require('./master/master-deposito');
const masterTabungan = require('./master/master-tabungan');
const masterPinjaman = require('./master/master-pinjaman');

const jenisDeposito = require('./parameter/jenis-deposito');
const jenisTabungan = require('./parameter/jenis-tabungan');
const jenisPinjaman = require('./parameter/jenis-pinjaman');

module.exports = {
    users,
    account,
    kelompok,
    jenis,
    bukubesar,
    anggota,
    masterDeposito,
    masterTabungan,
    masterPinjaman,
    jenisDeposito,
    jenisTabungan,
    jenisPinjaman
};