const pagging = (sql, params) => {
    console.log(params);
    var limit = params.limit || 100;
    var page = params.page || 1;
    var count = sql;

    if (params.paging == undefined || params.paging == "true") {
        return new Promise((resolve, reject) => {
            count.clearSelect()
                .count('*')
                .then((res) => {
                    sql
                        .offset((parseInt(limit) * parseInt(page)) - parseInt(limit))
                        .limit(parseInt(limit) || 100)
                        .orderBy(params.order_by, params.sort_by);
                    resolve({
                        sql: sql,
                        page: {
                            page: page,
                            total_page: parseInt(res[0].count) >= limit ? Math.round(parseInt(res[0].count) / limit, 10) : 1,
                            filtered_record: parseInt(res[0].count) >= limit ? limit : parseInt(res[0].count),
                            total_record: parseInt(res[0].count),
                            order_by: params.order_by,
                            sort_by: params.sort_by
                        }
                    });
                }).catch(err => {
                reject(err);
            });
        });
    } else {
        return new Promise((resolve, reject) => {
            count.clearSelect()
                .count('*')
                .then((res) => {
                    sql
                        .orderBy(params.order_by, params.sort_by);
                    resolve({
                        sql: sql,
                        page: {
                            page: 1,
                            total_page: 1,
                            filtered_record: parseInt(res[0].count),
                            total_record: parseInt(res[0].count),
                            order_by: params.order_by,
                            sort_by: params.sort_by
                        }
                    });
                }).catch(err => {
                reject(err);
            });
        });
    }

};


module.exports = {
    pagging
};