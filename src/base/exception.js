class BaseException extends Error {
    constructor(...args) {
        super(...args);
        this.statusCode = 200;
        this.publicMessage = "Succesfull";
        this.developerMessage = "Succesfull";
    }

    setStatusCode(status) {
        this.statusCode = status;
        return this;
    }

    setPublicMessage(message) {
        this.publicMessage = message;
        return this;
    }

    setDeveloperMessage(message) {
        this.developerMessage = message;
        return this;
    }

    setValidationError(err) {
        this.validationError = err;
        return this;
    }

    toJson() {
        return {
            statusCode: this.statusCode,
            publicMessage: this.publicMessage,
            developerMessage: this.developerMessage,
            validationError: this.validationError
        }
    }
}

class FailedTaskExecution extends BaseException {
    constructor(...args) {
        super(...args);
        this.statusCode = 503;
        if (args[0] != undefined) {
            this.publicMessage = args[0];
        } else {
            this.publicMessage = "Operation failed to execute.";
        }
        if (args[1] != undefined) {
            this.developerMessage = args[1];
        } else {
            this.developerMessage = "Operation failed to execute.";
        }
        Error.captureStackTrace(this, FailedTaskExecution);
    }
}

class NotFoundException extends BaseException {
    constructor(...args) {
        super(...args);
        this.statusCode = 404;
        if (args[0] != undefined) {
            this.publicMessage = args[0];
        } else {
            this.publicMessage = "Data Not Found.";
        }
        if (args[1] != undefined) {
            this.developerMessage = args[1];
        } else {
            this.developerMessage = "Data Not Found Check Validity you input.";
        }
        Error.captureStackTrace(this, NotFoundException);
    }
}
class UserNeedActivation extends BaseException {
    constructor(...args) {
        super(...args);
        this.statusCode = 402;
        if (args[0] != undefined) {
            this.publicMessage = args[0];
        } else {
            this.publicMessage = "User Tidak aktif, aktifkan terlebih dahulu.";
        }
        if (args[1] != undefined) {
            this.developerMessage = args[1];
        } else {
            this.developerMessage = "st = 0";
        }
        Error.captureStackTrace(this, UserNeedActivation);
    }
}

class UnauthorizerException extends BaseException {
    constructor(...args) {
        super(...args);
        this.statusCode = 401;
        if (args[0] != undefined) {
            this.publicMessage = args[0];
        } else {
            this.publicMessage = "Unauthorization Request.";
        }
        if (args[1] != undefined) {
            this.developerMessage = args[1];
        } else {
            this.developerMessage = "Invalid Credential.";
        }
        Error.captureStackTrace(this, UnauthorizerException);
    }
}

class ValidationException extends BaseException {
    constructor(...args) {
        super(...args);
        this.statusCode = 400;
        this.developerMessage = "wrong format request.";
        this.publicMessage = "missing mandatory data.";
        Error.captureStackTrace(this, ValidationException);
    }
}

class SystemError extends BaseException {
    constructor(...args) {
        super(...args);
        this.statusCode = 500;
        this.developerMessage = args[0];
        this.publicMessage = "server unavailable.";
        this.developerMessage = this.stack;
        Error.captureStackTrace(this, SystemError);
    }
}


const ExceptionMapper = [
    UnauthorizerException,
    ValidationException,
    SystemError,
    FailedTaskExecution,
    UserNeedActivation,
    NotFoundException
];

module.exports = {
    UnauthorizerException,
    ValidationException,
    ExceptionMapper,
    SystemError,
    FailedTaskExecution,
    UserNeedActivation,
    NotFoundException
};