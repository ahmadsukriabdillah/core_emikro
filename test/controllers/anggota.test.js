process.env.NODE_ENV = 'test';
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
const app = require('./../../app');
chai.use(chaiHttp);


describe('Anggota API Test', () => {
    var token;
    var resp;
    var payload = {
        nokartu: '-',
        nip: '',
        nogaji: '-',
        nama: 'Tarmudji',
        ki: '01',
        panggilan: '-',
        kl: 'L',
        tmp_lahir: '-',
        tgl_lahir: '0999-12-31T17:00:00.000Z',
        id: 'KTP',
        no_id: '-',
        tglexpired: '0999-12-31T17:00:00.000Z',
        agama: 'ISLAM',
        pendidikan: 'STRATA 1 (S-1)',
        pekerjaan: 'PEGAWAI NEGRI SIPIL (PNS)',
        jabatan: 'STAF',
        golongan: '-',
        alamat: '-',
        telepon: '-',
        handphone: '-',
        e_mail: '-',
        namaahliwaris: '-',
        hubkel: '-',
        alamatahliwaris: '-'
    };
    before((done) => {
        chai.request(app)
            .post('/api/auth')
            .send({
                username: "admin",
                password: "admin"
            })
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.data.profile.should.include.keys(
                    'cif', 'uid'
                );
                res.body.data.should.include.keys(
                    'token'
                );
                res.body.statusCode.should.eql(200);
                this.token = res.body.data.token;
                done();
            });
    });
    it('List Anggota', (done) => {
        chai.request(app)
            .get('/api/v1/anggota')
            .set('Authorization', this.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.should.include.keys(
                    'statusCode', 'data'
                );
                done();
            });
    });
    
    it('Create', (done) => {
        chai.request(app)
            .post('/api/v1/anggota')
            .send(this.payload)
            .set('Authorization', this.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.should.include.keys(
                    'statusCode', 'data'
                );
                this.resp = res.body.data;
                done();
            });
    });
    it('Detail Anggota', (done) => {
        chai.request(app)
            .get(`/api/v1/anggota/${this.resp.cib}`)
            .set('Authorization', this.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.should.include.keys(
                    'statusCode', 'data'
                );
                done();
            });
    });
    it('Update Anggota', done => {
        chai.request(app)
            .put(`/api/v1/anggota/${this.resp.cib}`)
            .send(this.payload)
            .set('Authorization', this.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.should.include.keys(
                    'statusCode'
                );
                done();
            });

    });
    it("Delete Anggota", done => {
        chai.request(app)
            .delete(`/api/v1/anggota/${this.resp.cib}`)
            .set('Authorization', this.token)
            .end((err, res) => {
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.should.include.keys(
                    'statusCode'
                );
                done();
            });
    })
});